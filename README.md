# 👀 Hi! I'm Olena Surilova!

## ⌨️ I'm Junior Front-End Developer

💻 Now I **continue to study Front-End development**
* JavaScript / HTML / CSS
* Vue.js
* React.js
* and other technologies

And also I keep looking for my **first dream job**

## A little bit about me

I'm from Ukraine 🇺🇦 I was born in Mykolaiv, it's a city in the south of Ukraine

Now, I have been lived in Greece for a year 🇬🇷 

My the biggest dream is about Peace! 🕊 
